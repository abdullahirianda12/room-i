function TaskModel() {
    const task = [];

    /**
     * addTask
     * @param {title: string} payload 
     */
    function addTask(payload) {
        task.push(payload);
    }

    /**
     * removeTask
     * @param index: number 
     */
    function removeTask(index) {
        console.log('deleted index :: ', index);
        task.splice(index, 1); // delete di array javascript
    }

    return {
        task,
        addTask,
        removeTask,
    }
}

function TodoApp() {
    const taskModel = TaskModel(); // variabel taskModel -> instansiasi

    const formAddTask = document.querySelector('section#section-todo-form > form');
    const titleInput = document.querySelector('input[name=title]');
    const sectionTodoItems = document.getElementById('section-todo-items');


    // perilaku / behavior / aktivitas
    function handleSubmit(e) {
        e.preventDefault();

        // tipe data Object {}
        const payload = {
            title: titleInput.value,
        };

        // taskModel menjadi array of object
        // object {title}
        taskModel.addTask(payload); //  di isi
        renderTaskItem();
		
		//Kosongkan Form
		titleInput.value='';
    }

    function renderTaskItem() {
        // isi taskModel di ubah
        // dari object[] -> string HTML[]
        // tipe data tetap Array [] 
        const renderItems = [];

        for (let i = 0; i < taskModel.task.length; i++) {
            renderItems.push(`
                <div class="todo-item my-3">
                    <span>
                        <strong id="title-item">${i}. ${taskModel.task[i].title} </strong>
                        <a style="float:right; href="#" class="remove-item" data-index="${i}" ><strong>X</strong></a>        
                    </span>
                </div>
            `);
        }

        sectionTodoItems.innerHTML = renderItems.join(' ');

        document.getElementsByClassName('button-delete')[0].innerHTML = `
            <a id="removeAll" href="">Hapus Semua</a>
        `;
        

        // populate setiap element yang punya class .remove-item
        document.querySelectorAll('.remove-item').forEach(function (removeItem) {
            // element html anchor a
            removeItem.onclick = function (event) {
                // proses parsing dari String ke Integer
                // dengan parsing 'memaksa' tipe data String menjadi Integer
                const indexTask = parseInt(event.target.getAttribute('data-index'));

                // lakukan remove task
                taskModel.removeTask(indexTask);

                // lakukan render ulang untuk menampilkan task item terbaru
                renderTaskItem();
            }
        });
    }
	
	formAddTask.onsubmit = handleSubmit;
	
}

TodoApp();




